/*
 * analyzer.cpp
 *
 *  Created on: May 4, 2018
 *      Author: jay
 */

#include <analyzer.h>
#include <stdio.h>

uint8_t RS = 31;
uint8_t ST = 30;

ADCDriver adc;
TickType_t  start_time;
TickType_t  current_time;
uint16_t val[7];

void analyzer::init()
{
    adc.adcSelectPin(adc.ADC_PIN_0_26);

    adc.adcInitBurstMode();

    LPC_GPIO1->FIODIR  |= (1 << RS) | (1 << ST);

}
void analyzer::reset_analyzer()
{

        LPC_GPIO1->FIOPIN |= (1 << RS);
        delay_us(1);

        LPC_GPIO1->FIOPIN &= ~(1 << RS);
        delay_us(72);

        LPC_GPIO1->FIOPIN |= (1 << ST);
}

uint16_t *analyzer::get_data()
{

    for(int i=0;i<7;i++)
    {

        LPC_GPIO1->FIOPIN &= ~(1 << ST);
        delay_us(40);
        val[i] = adc.readADCVoltageByChannel(3);
//        u0_dbg_printf("%4d ",val[i]);

        LPC_GPIO1->FIOPIN |= (1 << ST);
    }
//    u0_dbg_printf("\n");
    return val;
}

