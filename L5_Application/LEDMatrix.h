/*
 * LEDMatrix.h
 *
 *  Created on: Apr 23, 2018
 *      Author: jay
 */

#ifndef LEDMATRIX_H_
#define LEDMATRIX_H_
#include "gpio.hpp"

class LEDMatrix {
public:


    LEDMatrix();
    void init();
    void updateMatrix(uint8_t color,
            uint8_t select_line, uint8_t len1,uint8_t len2);
//    void reset_pin();
    virtual ~LEDMatrix();
};



#endif /* LEDMATRIX_H_ */
