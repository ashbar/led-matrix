/*
 * analyzer.h
 *
 *  Created on: May 4, 2018
 *      Author: jay
 */

#ifndef ANALYZER_H_
#define ANALYZER_H_
//#include <ADC.h>
#include "ADCDriver.hpp"
#include "gpio.hpp"
#include "utilities.h"
#include "printf_lib.h"
#include "tasks.hpp"

class analyzer {
public:

    void init();
    void reset_analyzer();
    uint16_t *get_data();

};



#endif /* ANALYZER_H_ */
