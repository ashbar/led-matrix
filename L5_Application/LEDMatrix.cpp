/*
 * LEDMatrix.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: jay
 */

#include <LEDMatrix.h>

uint8_t r1 = 29;
uint8_t b1 = 30;
uint8_t g1 = 7;
uint8_t r2 = 19;
uint8_t b2 = 20;
uint8_t g2 = 6;
uint8_t a = 22;
uint8_t b = 5;
uint8_t c = 23;
uint8_t clk = 28;
uint8_t oe = 29;
uint8_t lat = 4;


LEDMatrix::LEDMatrix()
{
    // TODO Auto-generated constructor stub

}

void LEDMatrix::init()
{

    LPC_GPIO0->FIODIR  |= (1 << r1) | (1 << b1);
    LPC_GPIO1->FIODIR  |= (1 << r2) | (1 << b2)|(1 << clk) | (1 << oe)|(1 << a) | (1 << c);
    LPC_GPIO2->FIODIR  |= (1 << b) | (1 << g1)|(1 << g2)|(1 << lat);

    LPC_GPIO1 ->FIOCLR = (1 << a);
    LPC_GPIO1 ->FIOCLR = (1 << b);
    LPC_GPIO1 ->FIOCLR = (1 << c);
    LPC_GPIO1->FIOCLR = (1 << clk);
    LPC_GPIO1->FIOCLR = (1 << oe);
    LPC_GPIO2->FIOCLR = (1 << lat);
}

void LEDMatrix::updateMatrix(uint8_t color,
        uint8_t select_line, uint8_t len1,uint8_t len2)
{

    LPC_GPIO1->FIOSET = (1 << oe);
    LPC_GPIO2->FIOSET = (1 << lat);

    (select_line & 0x1) ? LPC_GPIO1 ->FIOSET = (1 << a) : LPC_GPIO1 ->FIOCLR = (1 << a);
    (select_line & (0x1<<1)) ? LPC_GPIO2 ->FIOSET = (1 << b) : LPC_GPIO2 ->FIOCLR = (1 << b);
    (select_line & (0x1<<2)) ? LPC_GPIO1 ->FIOSET = (1 << c) : LPC_GPIO1 ->FIOCLR = (1 << c);

    for (int i = 32; i > 0; i--) {
        if (i > len1) {

            LPC_GPIO0 ->FIOCLR = (1 << r1) | (1 << b1);
            LPC_GPIO2 ->FIOCLR = (1 << g1);
        }
        else {
            (color &  0x1) ? LPC_GPIO0 ->FIOSET= (1 << r1) : LPC_GPIO0 ->FIOCLR = (1 << r1);
            (color & (0x1<<1)) ? LPC_GPIO0 ->FIOSET= (1 << b1) : LPC_GPIO0 ->FIOCLR = (1 << b1);
            (color & (0x1<<2)) ? LPC_GPIO2 ->FIOSET= (1 << g1) : LPC_GPIO2 ->FIOCLR = (1 << g1);
        }
        if (i > len2) {
            LPC_GPIO1 ->FIOCLR = (1 << r2) | (1 << b2);
            LPC_GPIO2 ->FIOCLR = (1 << g2);
        }
        else {
            (color &  0x1) ? LPC_GPIO1 ->FIOSET= (1 << r2) : LPC_GPIO1 ->FIOCLR = (1 << r2);
            (color & (0x1<<1)) ? LPC_GPIO1 ->FIOSET= (1 << b2) : LPC_GPIO1 ->FIOCLR = (1 << b2);
            (color & (0x1<<2)) ? LPC_GPIO2 ->FIOSET= (1 << g2) : LPC_GPIO2 ->FIOCLR = (1 << g2);

        }
        LPC_GPIO1->FIOSET = (1 << clk);
        LPC_GPIO1->FIOCLR = (1 << clk);

    }


    LPC_GPIO1->FIOCLR = (1 << oe);
    LPC_GPIO2->FIOCLR= (1 << lat);

}


LEDMatrix::~LEDMatrix()
{
    // TODO Auto-generated destructor stub
}

