/*
 * ADC.cpp
 *
 *  Created on: Mar 5, 2018
 *      Author: jay
 */

#include <ADC.h>

    /**
    * 1) Powers up ADC peripheral
    * 2) Set peripheral clock
    * 2) Enable ADC
    * 3) Select ADC channels
    * 4) Enable burst mode
    */
ADC::ADC()
{


}
void ADC::adcInitBurstMode()
{
    LPC_SC->PCONP |= (1 << 12);
    LPC_ADC->ADCR |= (1 << 21);
    LPC_ADC->ADCR |= (16<<8);  //modify
    LPC_SC->PCLKSEL0 |= (3<<24);
    LPC_ADC->ADCR |= (1<<16);  //Enable burst mode
}

void ADC::adcSelectPin(ADC_PIN adc_pin_arg)
{


    if(adc_pin_arg == ADC_PIN_0_25)
    {
//        LPC_ADC->ADCR |= (1<<2);
        LPC_PINCON->PINSEL1 &= ~(3<<18);
        LPC_PINCON->PINSEL1 |= (1<<18);
    }else if(adc_pin_arg == ADC_PIN_0_26)
    {
//        LPC_ADC->ADCR |= (1<<3);
        LPC_PINCON->PINSEL1 &= ~(3<<20);
        LPC_PINCON->PINSEL1 |= (1<<20);
    }else if(adc_pin_arg == ADC_PIN_1_30)
    {
//        LPC_ADC->ADCR |= (1<<4);
        LPC_PINCON->PINSEL3 |= (3<<28);
    }else if(adc_pin_arg == ADC_PIN_1_31)
    {
//        LPC_ADC->ADCR |= (1<<5);
        LPC_PINCON->PINSEL3 |= (3<<30);
    }


}
uint16_t ADC::readADCVoltageByChannel(uint8_t adc_channel_arg)
{
    uint16_t output_Voltage = 0;

    LPC_ADC->ADCR &= ~(7<<24); //Start bit should be 0
    if(adc_channel_arg < 8 && adc_channel_arg >= 0)
    {
    LPC_ADC->ADCR &= ~(0xFF);
    LPC_ADC->ADCR |= (1 << adc_channel_arg)|(1 << 24);
    output_Voltage = (LPC_ADC->ADGDR >> 4) & 0xFFF;
    }
//    u0_dbg_printf("%d   ",output_Voltage);
    return output_Voltage;
}

ADC::~ADC()
{
    // TODO Auto-generated destructor stub
}


