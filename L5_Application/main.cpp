/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 */

#include <stdio.h>
#include "utilities.h"
#include "io.hpp"
#include <LEDMatrix.h>
#include <analyzer.h>
LEDMatrix myled;
analyzer an;
int delay = 3;
int n = 128;
void vControlLED(void * pvParameters)
{

    uint16_t *val;

    uint16_t result;
    while (1) {
        val = an.get_data();

        for (int i = 0; i < 7; i++) {
//            u0_dbg_printf("%4d ",val[i]);

            result = val[i] * (0.9);

            if(result < 500)
                result = 0;

//            u0_dbg_printf("%4d ",result);

            if(i<3)
            {
                int j = i*2;
                myled.updateMatrix(i+1,j+1,result / n,0);
                vTaskDelay(delay);
                myled.updateMatrix(i+1,j+2,result / n,0);
                vTaskDelay(delay);
            }
            else if(i==3)
            {
                myled.updateMatrix(i+1,7,result / n,0);
                vTaskDelay(delay);
                myled.updateMatrix(i+1,0,0,result / n);
                vTaskDelay(delay);
            }
            else if(i>3)
            {
                int j = (i-4)*2;
                myled.updateMatrix(i+1,j+1,0,result / n);
                vTaskDelay(delay);
                myled.updateMatrix(i+1,j+2,0,result / n);
                vTaskDelay(delay);
            }
        }
        u0_dbg_printf("\n");
    }
}

int main(void)
{
    an.init();
    myled.init();
    TaskHandle_t xHandle = NULL;
    xTaskCreate(vControlLED, "LED", 2048, (void *) NULL, 1, &xHandle);
    vTaskStartScheduler();

}
