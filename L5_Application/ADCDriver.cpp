/*
 * ADCDriver.c
 *
 *  Created on: Mar 1, 2018
 *      Author: Sneha
 */
#include "ADCDriver.hpp"
#include "LPC17xx.h"
#include "printf_lib.h"
/**
 * 1) Powers up ADC peripheral
 * 2) Set peripheral clock
 * 2) Enable ADC
 * 3) Select ADC channels
 * 4) Enable burst mode
 */
void ADCDriver::adcInitBurstMode()
{

    //powering up the ADC
    LPC_SC->PCONP |= (1 << 12); //setting bit 12 in PCONP

    //setting the peripheral clock in PCLKSEL0
    LPC_SC->PCLKSEL0 &= ~(0x3 << 24); //set bit 24 and 25 to 0
    LPC_SC->PCLKSEL0 |= (3 << 24);  //set bit 25 and 24 to 0x11 to keep it at PCLK/8
    //PCLK/8 = 96MHz/8 = 12 MHz which is < 13 MHz (the max that ADC can handle)

    //enable the ADC - set PDN
    LPC_ADC->ADCR |= (1 << 21); //setting the PDN bit (bit 21) to enable the ADC


    //LPC_ADC->ADCR |= (1<<2); //setting bit 2 for AD 0.2

    //select the other channels AD 0.3, 0.4 and 0.5
    LPC_ADC->ADCR &= ~(1<<2); //unselect light sensor
    LPC_ADC->ADCR |= (1<<3);
    //LPC_ADC->ADCR |= (1<<2); //setting bit 2 for AD 0.2
    /*LPC_ADC->ADCR |= (1<<4);
    LPC_ADC->ADCR |= (1<<5);*/

    //enable burst mode
    //LPC_ADC->ADCR &= ~(7 << 24); //set start bits to 0
    LPC_ADC->ADCR |= (1 << 16);
    LPC_ADC->ADINTEN &= ~(1<<8);

    return;

}

/**
 * 1) Selects ADC functionality of any of the ADC pins that are ADC capable
 *
 * @param adc_pin_arg is the ADC_PIN enumeration of the desired pin.
 *
 * WARNING: For proper operation of the SJOne board, do NOT configure any pins
 *           as ADC except for 0.26, 1.31, 1.30
 */
void ADCDriver::adcSelectPin(ADC_PIN adc_pin_arg)
{

    //light sensor
    if (adc_pin_arg == ADC_PIN_0_25) {
        //select AD functionality for this port in PINSEL1
        LPC_PINCON->PINSEL1 |= (1 << 18); //setting bits 19 18 to 0 1 respectively for AD0.2
    }
    if (adc_pin_arg == ADC_PIN_0_26) {
        //select AD functionality for this port in PINSEL1
        LPC_PINCON->PINSEL1 &= ~(3 << 20); //clearing 20 and 21 bits
        LPC_PINCON->PINSEL1 |= (1 << 20); //setting bits 21 20 to 0 1 respectively for AD0.3
    }

    if (adc_pin_arg == ADC_PIN_1_30) {
        //select AD functionality for this port in PINSEL3
        LPC_PINCON->PINSEL3 |= (0x3 << 28); //setting bits 29 28 to 1 1 respectively for AD0.4
    }

    if (adc_pin_arg == ADC_PIN_1_31) {
        //select AD functionality for this port in PINSEL3
        LPC_PINCON->PINSEL3 |= (0x03 << 30); //setting bits 31 30 to 1 1 respectively for AD0.5
    }
    return;
}


float ADCDriver::readADCVoltageByChannel(uint8_t adc_channel_arg)
{
   /* uint32_t test1 = ((LPC_ADC->ADCR & (0xFF)));
               u0_dbg_printf("selected is %d\n",test1);
    uint32_t test = ((LPC_ADC->ADGDR & (7 << 24)) >> 24);
            u0_dbg_printf("last val is %d\n",test);*/
    if(adc_channel_arg ==2)
    {
        while(!(LPC_ADC->ADDR2 & (1 << 31))); //wait for it to be done. bit will be set to 1 when done
        return ((LPC_ADC->ADDR2 >> 4) & 0xFFF); //return the data
    }
    else if(adc_channel_arg == 3)
    {
        while(!(LPC_ADC->ADDR3 & (1 << 31))); //wait for it to be done. bit will be set to 1 when done
        return ((LPC_ADC->ADDR3 >> 4) & 0xFFF); //return the data
    }
    else if(adc_channel_arg == 4)
    {
        while(!(LPC_ADC->ADDR4 & (1 << 31))); //wait for it to be done. bit will be set to 1 when done
        return ((LPC_ADC->ADDR4 >> 4) & 0xFFF); //return the data
    }
    else if(adc_channel_arg == 5)
    {
        while(!(LPC_ADC->ADDR5 & (1 << 31))); //wait for it to be done. bit will be set to 1 when done
        return ((LPC_ADC->ADDR5 >> 4) & 0xFFF); //return the data
    }

    else
        return 0;

}
